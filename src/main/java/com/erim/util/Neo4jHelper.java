package com.erim.util;

import com.erim.domain.RelationshipTypes;
import com.google.common.collect.Lists;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.IndexManager;

import java.util.List;
import java.util.Map;

public abstract class Neo4jHelper {

    private final static String COUNT_KEY = "count";

    public static void createRelationship(Node startNode, Node endNode, RelationshipTypes relationshipType) {
        Relationship relationship = getRelationship(startNode, endNode, relationshipType);
        if(relationship == null){
           startNode.createRelationshipTo(endNode, relationshipType);
        }
    }

    public static void createRelationshipWithCounter(Node startNode, Node endNode, RelationshipTypes relationshipType) {

        Relationship relationship = getRelationship(startNode, endNode, relationshipType);

        if(relationship == null){
            relationship = startNode.createRelationshipTo(endNode, relationshipType);
        }

        if(relationship.hasProperty(COUNT_KEY)){
            Integer count = (Integer)relationship.getProperty(COUNT_KEY);
            relationship.setProperty(COUNT_KEY, count++);

        }else{
            relationship.setProperty(COUNT_KEY, 1);
        }

    }

    public static void cleanDb(GraphDatabaseService graphDatabaseService) {

        Transaction tx = graphDatabaseService.beginTx();
        try {
            removeNodes(graphDatabaseService);
            clearIndex(graphDatabaseService);
            tx.success();
        } finally {
            tx.finish();
        }
    }

    private static void removeNodes(GraphDatabaseService graphDatabaseService) {
        for (Node node : graphDatabaseService.getAllNodes()) {
            for (Relationship rel : node.getRelationships(Direction.OUTGOING)) {
                rel.delete();
            }
        }
        for (Node node : graphDatabaseService.getAllNodes()) {
                node.delete();
        }
    }

    private static void clearIndex(GraphDatabaseService gds) {
        IndexManager indexManager = gds.index();
        for (String ix : indexManager.nodeIndexNames()) {
            indexManager.forNodes(ix).delete();
        }
        for (String ix : indexManager.relationshipIndexNames()) {
            indexManager.forRelationships(ix).delete();
        }
    }

    public static boolean hasRelationship(Node start, Node end, RelationshipTypes rel) {

        Iterable<Relationship> relationships = start.getRelationships(rel, Direction.OUTGOING);

        for(Relationship relationship : relationships){
            if(relationship.getEndNode().equals(end)){
                return true;
            }
        }

        return false;
    }

    public static Relationship getRelationship(Node start, Node end, RelationshipTypes rel) {

        Iterable<Relationship> relationships = start.getRelationships(rel, Direction.OUTGOING);

        for(Relationship relationship : relationships){
            if(relationship.getEndNode().equals(end)){
                return relationship;
            }
        }

        return null;
    }

    public static Integer getRelationshipCounter(Node start, Node end, RelationshipTypes rel) {

        Iterable<Relationship> relationships = start.getRelationships(rel, Direction.OUTGOING);

        for(Relationship relationship : relationships){
            if(relationship.getEndNode().equals(end)){
                return (Integer)relationship.getProperty(COUNT_KEY);
            }
        }

        throw new RelationshipNotFounException();
    }

    public static List<String> iteratorToListBy(ResourceIterator<Map<String, Object>> iterator, String key) {

        List<String> result = Lists.newArrayList();
        while (iterator.hasNext()){
            Map<String, Object> next = iterator.next();
            String skuId = (String) next.get(key);
            result.add(skuId);
        }
        return result;

    }
}
