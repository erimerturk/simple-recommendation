package com.erim.test;

import com.erim.domain.dto.GenderType;
import com.erim.service.BuyerService;
import com.erim.service.CategoryService;
import com.erim.service.GenderService;
import com.erim.service.CreateGraphService;
import com.erim.service.ProductService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.lang.time.StopWatch;
import org.neo4j.graphdb.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

@Component
public class ApiTest {

    @Autowired
    private BuyerService buyerService;

    @Autowired
    private ProductService skuService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private CreateGraphService orderService;

    @Autowired
    private GenderService genderService;

    @Resource(name="sqlProperties")
    private Properties sqlProperties;

    public void createAll(){

        Node male = genderService.findBy(GenderType.MALE.toString());
        Node female = genderService.findBy(GenderType.FEMALE.toString());

        try {
            Connection conn = dataSource.getConnection();
//            PreparedStatement ps = conn.prepareStatement("select o.buyer_id, p.id, p.category_id, buyer.gender, o.id as orderId from order_item item, orders o, product p, sku s, buyer buyer where item.orderitemstatus = 'COMPLETED' and item.order_id = o.id and item.sku_id = s.id and s.product_id =p.id and item.category_id in (select id from category where categorygroup = 'FASHION_BEAUTY_AND_JEWELLERY') and o.buyer_id = buyer.id");
            PreparedStatement ps = conn.prepareStatement(sqlProperties.getProperty("createForFashion"));
            ResultSet resultSet = ps.executeQuery();

            StopWatch stopWatch = new StopWatch();

            System.out.println("it started");
            int i = 0;

            stopWatch.start();
            while(resultSet.next()){
                i++;

                Node genderNode = null;

                String gender = resultSet.getString("gender");
                if(StringUtils.isNotBlank(gender)){
                    genderNode = "F".equals(gender) ? female : male;
                }
                orderService.createOrder(resultSet.getString("category_id"), resultSet.getString("buyer_id"), resultSet.getString("id"), genderNode, resultSet.getString("orderId"));

                if(i % 1000 == 0){
                    System.out.println(i + " persisted");
                }

            }
            System.out.println(i + " finished");

            stopWatch.stop();


            System.out.println("It takes : " + stopWatch.getTime());

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    @Transactional
    public void createCategoryTree(){

        try {
            Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sqlProperties.getProperty("createCategoryTreeForFashion"));
            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()){
                categoryService.createCategoryBy(resultSet.getString("id"), resultSet.getString("parent_id"));
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void createAllBuyer(){

        Node male = genderService.create(GenderType.MALE.toString());
        Node female = genderService.create(GenderType.FEMALE.toString());

        try {
            Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sqlProperties.getProperty("createAllBuyers"));
            ResultSet resultSet = ps.executeQuery();

            int i = 0;

            while(resultSet.next()){
                i++;
                Node genderNode = null;

                String gender = resultSet.getString("gender");
                if(StringUtils.isNotBlank(gender)){
                    genderNode = "F".equals(gender) ? female : male;
                }

                orderService.createBuyer(resultSet.getString("buyer_id"), genderNode);

                if(i % 1000 == 0){
                    System.out.println(i + " persisted");
                }
            }

            System.out.println(i + " finished");

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void updateDeletedProduct(){

        try {
            Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sqlProperties.getProperty("updateDeletedProduct"));
            ResultSet resultSet = ps.executeQuery();

            int i = 0;

            while(resultSet.next()){
                i++;

                skuService.updateProduct(resultSet.getString("id"), resultSet.getBoolean("deleted"));

                if(i % 1000 == 0){
                    System.out.println(i + " persisted");
                }
            }

            System.out.println(i + " finished");

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Transactional
    public void createCategories(){

        for(int i=0; i< 5; i++){
            String categoryId = "12" + i;
            categoryService.createCategoryBy(categoryId);



        }

//        ExecutionEngine engine = new ExecutionEngine(graphDatabaseService);
//
//       String cql = "start doctor = node:characters(character = 'Doctor')"
//                + "match (doctor)<-[:PLAYED]-()-[regeneratedRelationship:REGENERATED_TO]->()"
//                + "return max(regeneratedRelationship.year) as latestRegenerationYear";
//
//
//        ExecutionResult result = engine.execute(cql);

    }


    @Transactional
    public void createSkus(){


        for(int i=0; i< 10; i++){
            Node category = findRandomCategory();
            String skuId = "123" + i;
            skuService.createSkuBy(skuId, category);
        }
    }

    private Node findRandomCategory() {
        int index = RandomUtils.nextInt(5);
        String categoryId = "12" + index;
        return categoryService.findCategoryBy(categoryId);
    }

    private Node findRandomSku() {
        int index = RandomUtils.nextInt(10);
        String skuId = "123" + index;
        return skuService.findBy(skuId);
    }

    @Transactional
    public void createBuyers(){


        for(int i=0; i< 100; i++){

            Node buyer = buyerService.createBuyerBy("b1" + i);
            Node sku = findRandomSku();
            buyerService.orderSku(buyer, sku, null);

        }
    }

    public void insertParentCategories() {

        try {
            Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sqlProperties.getProperty("insertParentCategoryForFashion"));
            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()){
                categoryService.createCategoryBy(resultSet.getString("id"));
                System.out.println("created parent id = " + resultSet.getString("id"));
            }

            resultSet.close();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
