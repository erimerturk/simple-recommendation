package com.erim.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class InitTestValues {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "app-context.xml");

        ApiTest application = (ApiTest) context.getBean("apiTest");

        application.createCategories();
        application.createSkus();
        application.createBuyers();

    }

}
