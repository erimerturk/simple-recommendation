package com.erim.service;

import com.erim.domain.dto.CategoryNode;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.helpers.collection.IteratorUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoryService extends GraphService {

    @Transactional
    public Node createCategoryBy(String categoryId) {
        Node category = graphDatabaseService.createNode(CategoryNode.CATEGORY);
        category.setProperty(CategoryNode.CATEGORY_ID, categoryId);
        return category;
    }

    public Node findCategoryBy(String id) {
        ResourceIterator<Node> category = graphDatabaseService.findNodesByLabelAndProperty(CategoryNode.CATEGORY, CategoryNode.CATEGORY_ID, id).iterator();
        return IteratorUtil.singleOrNull(category);
    }

    @Transactional
    public void insertOrNot(String categoryId) {

        Node categoryBy = findCategoryBy(categoryId);
        if(categoryBy == null){
            createCategoryBy(categoryId);
        }
    }

    @Transactional
    public void createCategoryBy(String catId, String parentId) {
        Node current = findCategoryBy(catId);
        Node parent = findCategoryBy(parentId);

        if(parent == null){
            System.out.println("not found parent id : " +parentId);
            parent = createCategoryBy(parentId);
        }

        if(current == null){
            current = createCategoryBy(catId);
        }

        if(!CategoryNode.hasChildRel(parent, current)){
            CategoryNode.createChildRelationship(parent, current);
        }

    }
}
