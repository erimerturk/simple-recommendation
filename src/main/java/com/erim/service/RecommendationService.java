package com.erim.service;

import com.erim.domain.dto.BuyerNode;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class RecommendationService {

    @Autowired
    private CypherExecutorService cypherExecutorService;

    @Transactional
    public List<String> recommendByProductId(String productId) {

        Map<String, Object> params = Maps.newHashMap();
        params.put("productId", productId);
        return cypherExecutorService.executeWithParams("recommendByProductId", params, "skuId");
    }

    public List<String> recommendByBuyerTransactionHistory(String buyerId) {

        Map<String, Object> params = Maps.newHashMap();
        params.put(BuyerNode.BUYER_ID, buyerId);
        return cypherExecutorService.executeWithParams("recommendByBuyerTransactionHistory", params, "skuId");
    }

    public List<String> recommendByBuyerWatchList(String buyerId, List<String> products) {

        Map<String, Object> params = Maps.newHashMap();
        params.put(BuyerNode.BUYER_ID, buyerId);
        params.put("pList", products);
        return cypherExecutorService.executeWithParams("recommendByBuyerWatchList", params, "skuId");
    }

    public List<String> recommendByBuyerTransactionHistoryBasedOnGender(String buyerId, String genderType) {

        Map<String, Object> params = Maps.newHashMap();
        params.put(BuyerNode.BUYER_ID, buyerId);
        params.put("gender", genderType);
        return cypherExecutorService.executeWithParams("recommendByBuyerTransactionHistoryBasedOnGender", params, "skuId");
    }

    public List<String> recommendBy(String buyerId) {

        Map<String, Object> params = Maps.newHashMap();
        params.put(BuyerNode.BUYER_ID, buyerId);
        return cypherExecutorService.executeWithParams("recommendBy", params, "skuId");
    }

}
