package com.erim.service;

import com.erim.domain.dto.OrderNode;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.helpers.collection.IteratorUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrderService extends GraphService {

    @Transactional
    public Node createOrderBy(String orderId) {
        Node order = graphDatabaseService.createNode(OrderNode.ORDER);
        order.setProperty(OrderNode.ORDER_ID, orderId);
        return order;
    }

    public Node findOrderBy(String id) {
        ResourceIterator<Node> order = graphDatabaseService.findNodesByLabelAndProperty(OrderNode.ORDER, OrderNode.ORDER_ID, id).iterator();
        return IteratorUtil.singleOrNull(order);
    }

    @Transactional
    public void createOrderItemRelation(Node order, Node product) {
        OrderNode.createOrderItemRelationship(order, product);
    }
}
