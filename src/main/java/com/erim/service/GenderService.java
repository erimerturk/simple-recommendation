package com.erim.service;

import com.erim.domain.dto.GenderNode;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.helpers.collection.IteratorUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GenderService extends GraphService{

    @Transactional
    public Node create(String genderType) {
            Node gender = graphDatabaseService.createNode(GenderNode.GENDER);
            gender.setProperty(GenderNode.GENDER_TYPE, genderType);
            return gender;

    }

    @Transactional
    public Node findBy(String genderType) {
        ResourceIterator<Node> order = graphDatabaseService.findNodesByLabelAndProperty(GenderNode.GENDER, GenderNode.GENDER_TYPE, genderType).iterator();
        return IteratorUtil.singleOrNull(order);

    }
}
