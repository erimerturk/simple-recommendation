package com.erim.service;

import com.erim.domain.dto.BuyerNode;
import org.neo4j.graphdb.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CreateGraphService {

    @Autowired
    private BuyerService buyerService;

    @Autowired
    private ProductService skuService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private OrderService orderService;


    @Transactional
    public void createOrder(String categoryId, String buyerId, String skuId, Node gender, String orderId){


        Node buyer = buyerService.findBy(buyerId);

        Node category = categoryService.findCategoryBy(categoryId);
        Node order = orderService.findOrderBy(orderId);

        if(category == null){
            category = categoryService.createCategoryBy(categoryId);
        }

        Node product = skuService.findBy(skuId);

        if(product == null){
            product = skuService.createSkuBy(skuId, category);
        }

        if(order == null){

            order = orderService.createOrderBy(orderId);
        }

        orderService.createOrderItemRelation(order, product);

        buyerService.orderSku(buyer, product, order);

    }


    @Transactional
    public void createBuyer(String buyerId, Node gender){



        Node buyer = buyerService.createBuyerBy(buyerId);


        if(gender != null){
            BuyerNode.createGenderRelationship(buyer, gender);
        }

    }
}
