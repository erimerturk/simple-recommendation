package com.erim.service;

import com.erim.util.Neo4jHelper;
import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.graphdb.ResourceIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Service
public class CypherExecutorService extends GraphService {

    @Resource(name="cypherProperties")
    private Properties cypherProperties;

    @Autowired
    private ExecutionEngine executionEngine;

    @Transactional
    public ResourceIterator<Map<String, Object>> executeWithParams(final String cqlKey, Map<String, Object> params) {
        return executionEngine.execute(cypherProperties.getProperty(cqlKey), params).iterator();
    }

    @Transactional
    public List<String> executeWithParams(final String cqlKey,final Map<String, Object> params, final String returnKey) {
        ResourceIterator<Map<String, Object>> iterator = executionEngine.execute(cypherProperties.getProperty(cqlKey), params).iterator();
        return Neo4jHelper.iteratorToListBy(iterator, returnKey);
    }

}
