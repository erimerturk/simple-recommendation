package com.erim.service;

import com.erim.domain.dto.BaseNode;
import com.erim.domain.dto.BuyerNode;
import com.erim.domain.dto.CategoryNode;
import com.erim.domain.dto.OrderNode;
import com.erim.domain.dto.ProductNode;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.schema.Schema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

@Service
public class NodeSaver {

    @Autowired
    private GraphDatabaseService neo;

    @PostConstruct
    @Transactional
    public void init(){


//        Transaction tx = neo.beginTx();
        try {
        Schema schema = neo.schema();
        schema.indexFor(BuyerNode.BUYER)
                .on("buyerId")
                .create();

        schema.indexFor(ProductNode.PRODUCT)
                .on("skuId")
                .create();

        schema.indexFor(CategoryNode.CATEGORY)
                .on("categoryId")
                .create();

        schema.indexFor(OrderNode.ORDER)
                .on(OrderNode.ORDER_ID)
                .create();


//            tx.success();
        }catch (Exception e){
            System.out.println("index create patladi : " + e.getLocalizedMessage());
        }

    }

    @Transactional
    public <T extends BaseNode> Node save(T input){

        Node node = neo.createNode(input.getLabel());
        input.copyTo(node);
        node.setProperty(BaseNode.DELETED, input.isDeleted());

        return node;

    }
}
