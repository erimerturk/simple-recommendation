package com.erim.service;

import com.erim.domain.Buyer;
import com.erim.domain.dto.BuyerNode;
import com.erim.domain.dto.ProductNode;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterable;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.helpers.collection.IteratorUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BuyerService extends GraphService{

    @Transactional
    public Node createBuyerBy(String buyerId) {

        Node buyer = graphDatabaseService.createNode(BuyerNode.BUYER);
        buyer.setProperty(BuyerNode.BUYER_ID, buyerId);

        return buyer;

    }

    @Transactional
    public void orderSku(Node buyer, Node sku, Node order) {

        BuyerNode.createOrderedProductRelationship(buyer, sku);
        Node category = ProductNode.getCategoryRelationship(sku);
        BuyerNode.createOrderedCategoryRelationship(buyer, category);
        BuyerNode.createOrderedRelationship(buyer, order);

    }

    @Transactional
    public Node findBy(String buyerId) {
        ResourceIterable<Node> fromNeo = graphDatabaseService.findNodesByLabelAndProperty(BuyerNode.BUYER, BuyerNode.BUYER_ID, buyerId);
        Node single = IteratorUtil.singleOrNull(fromNeo);
        return single;
    }


    @Transactional
    public Buyer findBuyerNode(String id) {

        ResourceIterator<Node> buyers = graphDatabaseService.findNodesByLabelAndProperty(BuyerNode.BUYER, BuyerNode.BUYER_ID, id).iterator();
        Node node = IteratorUtil.singleOrNull(buyers);

        if(node != null){
            return BuyerNode.toBuyer(node);
        }
        return null;

    }

}
