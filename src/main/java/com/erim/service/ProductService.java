package com.erim.service;

import com.erim.domain.Product;
import com.erim.domain.RelationshipTypes;
import com.erim.domain.dto.ProductNode;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.helpers.collection.IteratorUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductService extends GraphService{


    @Transactional
    public Node createSkuBy(String skuId, Node category) {
        Node sku = graphDatabaseService.createNode(ProductNode.PRODUCT);
        sku.setProperty(ProductNode.PRODUCT_ID, skuId);
        sku.createRelationshipTo(category, RelationshipTypes.CATEGORY);

        return sku;

    }

    public Node findBy(String id) {
        ResourceIterator<Node> sku = graphDatabaseService.findNodesByLabelAndProperty(ProductNode.PRODUCT, ProductNode.PRODUCT_ID, id).iterator();
        return IteratorUtil.singleOrNull(sku);
    }

    @Transactional
    public Product findProductNode(String id) {

        ResourceIterator<Node> sku = graphDatabaseService.findNodesByLabelAndProperty(ProductNode.PRODUCT, ProductNode.PRODUCT_ID, id).iterator();
        Node node = IteratorUtil.singleOrNull(sku);

        if(node != null){
            return ProductNode.toProduct(node);
        }
        return null;

    }

    @Transactional
    public void updateProduct(String id, boolean deleted) {

        Node product = findBy(id);
        product.setProperty(ProductNode.DELETED, deleted);

    }
}
