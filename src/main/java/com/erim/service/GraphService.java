package com.erim.service;

import org.neo4j.graphdb.GraphDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class GraphService{

    @Autowired
    protected GraphDatabaseService graphDatabaseService;

    public GraphDatabaseService getGraphDatabaseService() {
        return graphDatabaseService;
    }

}
