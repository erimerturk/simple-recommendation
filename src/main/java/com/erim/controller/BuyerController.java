package com.erim.controller;

import com.erim.domain.Buyer;
import com.erim.service.BuyerService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/buyer")
public class BuyerController {

    @Autowired
    private BuyerService buyerService;

    @RequestMapping(value = "/{buyerId}", method = RequestMethod.GET)
    @ResponseBody
    public Buyer findBy(@PathVariable("buyerId") String buyerId) {

        if(StringUtils.isNotBlank(buyerId)){
            return buyerService.findBuyerNode(buyerId);
        }
        return null;
    }



}
