package com.erim.controller;

import com.erim.service.RecommendationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/rec")
public class RecommendationController {

    @Autowired
    private RecommendationService recommendationService;

    @RequestMapping(value = "/history/{buyerId}", method = RequestMethod.GET)
    @ResponseBody
    public List<String> findBy(@PathVariable("buyerId") String buyerId) {
        return recommendationService.recommendByBuyerTransactionHistory(buyerId);

    }

    @RequestMapping(value = "/most/{buyerId}", method = RequestMethod.GET)
    @ResponseBody
    public List<String> most(@PathVariable("buyerId") String buyerId) {
        return recommendationService.recommendBy(buyerId);

    }



}
