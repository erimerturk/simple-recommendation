package com.erim.controller;

import com.erim.service.RecommendationService;
import com.erim.test.ApiTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/welcome")
public class HelloController {

    @Autowired
    private ApiTest apiTest;

    @Autowired
    private RecommendationService recommendationService;

    @RequestMapping(value = "/insertParent", method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {

        model.addAttribute("message", "Spring 3 MVC Hello World");

        apiTest.insertParentCategories();
        apiTest.createCategoryTree();
        apiTest.createAllBuyer();

        return "hello";

    }

    @RequestMapping(value = "/createTree", method = RequestMethod.GET)
    public String createTree(ModelMap model) {

        model.addAttribute("message", "Spring 3 MVC Hello World");

        apiTest.createCategoryTree();

        return "hello";

    }

    @RequestMapping(value = "/createAll", method = RequestMethod.GET)
    public String createAll(ModelMap model) {

        model.addAttribute("message", "Spring 3 MVC Hello World");

        apiTest.createAll();

        return "hello";

    }

    @RequestMapping(value = "/recom/{buyerId}", method = RequestMethod.GET)
    @ResponseBody
    public List<String> findBy(@PathVariable("buyerId") String buyerId) {
        return recommendationService.recommendByBuyerTransactionHistory(buyerId);

    }



}
