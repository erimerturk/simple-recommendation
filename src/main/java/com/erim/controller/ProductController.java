package com.erim.controller;

import com.erim.domain.Product;
import com.erim.service.ProductService;
import com.erim.test.ApiTest;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ApiTest apiTest;

    @RequestMapping(value = "/{productId}", method = RequestMethod.GET)
    @ResponseBody
    public Product findBy(@PathVariable("productId") String productId) {

        if(StringUtils.isNotBlank(productId)){
            return productService.findProductNode(productId);
        }
        return null;
    }

    @RequestMapping(value = "/updateDeleted", method = RequestMethod.GET)
    public String updateDeleted() {

        apiTest.updateDeletedProduct();

        return "hello";

    }



}
