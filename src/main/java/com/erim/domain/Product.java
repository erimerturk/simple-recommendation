package com.erim.domain;

import com.google.common.collect.Sets;

import java.util.Set;

public class Product {

    private Long id;

    private String productId;

    private Category category;

    private Set<String> buyers = Sets.newHashSet();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<String> getBuyers() {
        return buyers;
    }

    public void setBuyers(Set<String> buyers) {
        this.buyers = buyers;
    }
}
