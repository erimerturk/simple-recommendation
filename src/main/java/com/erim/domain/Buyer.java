package com.erim.domain;

import com.erim.domain.dto.GenderType;
import com.google.common.collect.Sets;

import java.util.HashSet;
import java.util.Set;

public class Buyer {

    private Long id;

    private String buyerId;

    private Set<Product> orders = Sets.newHashSet();
    private Set<Product> recommendedSkus = new HashSet<Product>();

    private Set<Category> orderedCategories = new HashSet<Category>();

    private GenderType gender;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public Set<Product> getOrders() {
        return orders;
    }

    public void setOrders(Set<Product> orders) {
        this.orders = orders;
    }

    public Set<Product> getRecommendedSkus() {
        return recommendedSkus;
    }

    public void setRecommendedSkus(Set<Product> recommendedSkus) {
        this.recommendedSkus = recommendedSkus;
    }

    public Set<Category> getOrderedCategories() {
        return orderedCategories;
    }

    public void setOrderedCategories(Set<Category> orderedCategories) {
        this.orderedCategories = orderedCategories;
    }

    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }
}
