package com.erim.domain;

import org.neo4j.graphdb.RelationshipType;

public enum RelationshipTypes implements RelationshipType {

    ORDERED_PRODUCT,
    CHILD_CATEGORY,
    ORDERED_CATEGORIES,
    ORDER_ITEM,
    GENDER,
    CATEGORY,
    RECOMMENDED,
    ORDERED,
}
