package com.erim.domain.dto;

import com.erim.domain.Category;
import com.erim.domain.Product;
import com.erim.domain.RelationshipTypes;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.helpers.collection.IteratorUtil;

public class ProductNode extends BaseNode {

    public static final Label PRODUCT = DynamicLabel.label("Sku");
    public static final String PRODUCT_ID = "skuId";

    private Long id;

    private String skuId;

    private Node category;

    public Long getId() {
        return id;
    }

    @Override
    public Label getLabel() {
        return PRODUCT;
    }

    @Override
    public Node copyTo(Node node) {
        node.setProperty(PRODUCT_ID, skuId);
        return node;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public static  String getProductId(Node sku) {
        return (String)sku.getProperty(PRODUCT_ID);
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public Node getCategory() {
        return category;
    }

    public void setCategory(Node category) {
        this.category = category;
    }

    public static Node getCategoryRelationship(Node sku) {
        Relationship categoryRel = IteratorUtil.singleOrNull(sku.getRelationships(RelationshipTypes.CATEGORY));
        if(categoryRel != null){
            return categoryRel.getEndNode();
        }
        return null;
    }

    public static Product toProduct(Node node) {

        Product product = new Product();
        product.setId(node.getId());
        product.setProductId(getProductId(node));

        Node categoryRelationship = getCategoryRelationship(node);

        Iterable<Relationship> ordered = node.getRelationships(RelationshipTypes.ORDERED_PRODUCT);
        for (Relationship order : ordered) {
//            product.getBuyers().add(BuyerNode.getBuyerId(order.getStartNode()));
        }
        if(categoryRelationship != null){
            Category category = CategoryNode.toCategory(categoryRelationship);
            product.setCategory(category);
        }
        return product;
    }
}
