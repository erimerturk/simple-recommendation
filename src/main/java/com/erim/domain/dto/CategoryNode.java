package com.erim.domain.dto;

import com.erim.domain.Category;
import com.erim.domain.RelationshipTypes;
import com.erim.util.Neo4jHelper;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import java.util.HashSet;
import java.util.Set;

public class CategoryNode extends BaseNode {

    public static final Label CATEGORY = DynamicLabel.label("Category");
    public static final String CATEGORY_ID = "categoryId";

    private Long id;

    private String categoryId;

    private Set<Node> childCategories = new HashSet<Node>();

    public Long getId() {
        return id;
    }

    @Override
    public Label getLabel() {
        return CATEGORY;
    }

    @Override
    public Node copyTo(Node node) {

        node.setProperty(CATEGORY_ID, categoryId);

        for(Node endNode : childCategories){
            createChildRelationship(node, endNode);
        }

        return node;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Set<Node> getChildCategories() {
        return childCategories;
    }

    public void setChildCategories(Set<Node> childCategories) {
        this.childCategories = childCategories;
    }

    public static boolean hasChildRel(Node start, Node end) {

        Iterable<Relationship> relationships = start.getRelationships(RelationshipTypes.CHILD_CATEGORY);

        for(Relationship relationship : relationships){
            if(relationship.getEndNode().equals(end)){
                return true;
            }
        }

        return false;
    }

    public static void createChildRelationship(Node startNode, Node endNode) {
        Neo4jHelper.createRelationship(startNode, endNode, RelationshipTypes.CHILD_CATEGORY);
    }

    public static Category toCategory(Node node) {
        Category category = new Category();
        category.setId(node.getId());
        category.setCategoryId((String)node.getProperty(CATEGORY_ID));
        return category;
    }
}
