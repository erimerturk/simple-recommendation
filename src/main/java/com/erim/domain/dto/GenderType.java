package com.erim.domain.dto;

public enum GenderType {
    MALE, FEMALE
}
