package com.erim.domain.dto;

import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;

public abstract class BaseNode {

    public static final String DELETED = "deleted";
    private boolean deleted;

    public abstract void setId(Long id);
    public abstract Long getId();
    public abstract Label getLabel();
    public abstract Node copyTo(Node node);

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }

        final BaseNode otherEntity = (BaseNode) other;
        if (getId() == null) {
            if (otherEntity.getId() != null) {
                return false;
            }
        } else if (!getId().equals(otherEntity.getId())) {
            return false;
        }
        return true;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
