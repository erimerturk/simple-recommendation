package com.erim.domain.dto;

import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;

public class GenderNode extends BaseNode {

    public static final Label GENDER = DynamicLabel.label("Gender");
    public static final String GENDER_TYPE = "genderType";

    private Long id;

    private GenderType genderType;

    public Long getId() {
        return id;
    }

    @Override
    public Label getLabel() {
        return GENDER;
    }

    @Override
    public Node copyTo(Node node) {
        node.setProperty(GENDER_TYPE, genderType.toString());
        return node;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public static String getGenderType(Node node) {
        return (String)node.getProperty(GENDER_TYPE);
    }

    public static GenderType getGenderTypeAsEnum(Node node) {
        return GenderType.valueOf((String) node.getProperty(GENDER_TYPE));
    }

    public void setGenderType(GenderType genderType) {
        this.genderType = genderType;
    }
}
