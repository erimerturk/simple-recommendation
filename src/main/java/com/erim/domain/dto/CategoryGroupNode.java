package com.erim.domain.dto;

import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;

import java.util.HashSet;
import java.util.Set;

public class CategoryGroupNode extends BaseNode {

    public static final Label CATEGORYGROUP = DynamicLabel.label("CategoryGroup");
    public static final String CATEGORY_GROUP_ID = "categoryGroupId";

    private Long id;

    private String categoryGroupId;

    private Set<CategoryNode> childCategories = new HashSet<CategoryNode>();

    public Long getId() {
        return id;
    }

    @Override
    public Label getLabel() {
        return CATEGORYGROUP;
    }

    @Override
    public Node copyTo(Node node) {

        node.setProperty(CATEGORY_GROUP_ID, categoryGroupId);
        return node;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryGroupId() {
        return categoryGroupId;
    }

    public void setCategoryGroupId(String categoryGroupId) {
        this.categoryGroupId = categoryGroupId;
    }

    public Set<CategoryNode> getChildCategories() {
        return childCategories;
    }

    public void setChildCategories(Set<CategoryNode> childCategories) {
        this.childCategories = childCategories;
    }
}
