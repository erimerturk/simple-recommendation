package com.erim.domain.dto;

import com.erim.domain.Buyer;
import com.erim.domain.Category;
import com.erim.domain.Product;
import com.erim.domain.RelationshipTypes;
import com.erim.util.Neo4jHelper;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import java.util.HashSet;
import java.util.Set;

public class BuyerNode extends BaseNode {

    public static final Label BUYER = DynamicLabel.label("Buyer");
    public static final String BUYER_ID = "buyerId";

    private Long id;

    private String buyerId;

    private Set<Node> orders = new HashSet<Node>();
    private Set<Node> recommendedSkus = new HashSet<Node>();

    private Set<Node> orderedCategories = new HashSet<Node>();

    private Node genderNode;


    public Long getId() {
        return id;
    }

    @Override
    public Label getLabel() {
        return BUYER;
    }

    @Override
    public Node copyTo(Node node) {

        node.setProperty(BUYER_ID, buyerId);

        for(Node category : orderedCategories){
            createOrderedCategoryRelationship(node, category);
        }

        for(Node sku : orders){
            createOrderedProductRelationship(node, sku);
        }

        for(Node recommended : recommendedSkus){
            createRecommendedSkuRelationship(node, recommended);
        }

        if(genderNode != null){
            createGenderRelationship(node, genderNode);
        }

        return node;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public static String getBuyerId(Node node) {
        return (String)node.getProperty(BUYER_ID);
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public Set<Node> getOrders() {
        return orders;
    }

    public void setOrders(Set<Node> orders) {
        this.orders = orders;
    }

    public Set<Node> getOrderedCategories() {
        return orderedCategories;
    }

    public void setOrderedCategories(Set<Node> orderedCategories) {
        this.orderedCategories = orderedCategories;
    }

    public Node getGenderNode() {
        return genderNode;
    }

    public void setGenderNode(Node genderNode) {
        this.genderNode = genderNode;
    }

    public Set<Node> getRecommendedSkus() {
        return recommendedSkus;
    }

    public void setRecommendedSkus(Set<Node> recommendedSkus) {
        this.recommendedSkus = recommendedSkus;
    }

    public static boolean hasOrderedCategoryRel(Node buyer, Node category) {
        return Neo4jHelper.hasRelationship(buyer, category, RelationshipTypes.ORDERED_CATEGORIES);
    }

    public static boolean hasOrderedProductRel(Node buyer, Node sku) {
        return Neo4jHelper.hasRelationship(buyer, sku, RelationshipTypes.ORDERED_PRODUCT);
    }

    public static boolean hasRecommendedSkudRel(Node buyer, Node sku) {
        return Neo4jHelper.hasRelationship(buyer, sku, RelationshipTypes.RECOMMENDED);
    }

    public static boolean hasGenderdRel(Node buyer, Node gender) {
        return Neo4jHelper.hasRelationship(buyer, gender, RelationshipTypes.GENDER);
    }

    public static void createOrderedCategoryRelationship(Node buyer, Node category) {
        Neo4jHelper.createRelationshipWithCounter(buyer, category, RelationshipTypes.ORDERED_CATEGORIES);
    }

    public static void createRecommendedSkuRelationship(Node buyer, Node sku) {
        Neo4jHelper.createRelationship(buyer, sku, RelationshipTypes.RECOMMENDED);
    }

    public static void createOrderedProductRelationship(Node buyer, Node sku) {
        Neo4jHelper.createRelationshipWithCounter(buyer, sku, RelationshipTypes.ORDERED_PRODUCT);
    }

    public static void createOrderedRelationship(Node buyer, Node order) {
        Neo4jHelper.createRelationship(buyer, order, RelationshipTypes.ORDERED);
    }

    public static void createGenderRelationship(Node buyer, Node gender) {
        Neo4jHelper.createRelationship(buyer, gender, RelationshipTypes.GENDER);
    }

    public static Buyer toBuyer(Node node) {
        Buyer buyer = new Buyer();
        buyer.setId(node.getId());
        buyer.setBuyerId(getBuyerId(node));

        Relationship gender = node.getSingleRelationship(RelationshipTypes.GENDER, Direction.INCOMING);

        if(gender != null){
            Node genderNode = gender.getEndNode();
            buyer.setGender(GenderNode.getGenderTypeAsEnum(genderNode));
        }

        Iterable<Relationship> relationships = node.getRelationships(RelationshipTypes.ORDERED_PRODUCT);

        for (Relationship relationship : relationships) {
            Product product = ProductNode.toProduct(relationship.getEndNode());
            buyer.getOrders().add(product);
        }

        Iterable<Relationship> recommendeds = node.getRelationships(RelationshipTypes.RECOMMENDED);

        for (Relationship recommend : recommendeds) {
            Product product = ProductNode.toProduct(recommend.getEndNode());
            buyer.getRecommendedSkus().add(product);
        }

        Iterable<Relationship> orderedCategories = node.getRelationships(RelationshipTypes.ORDERED_CATEGORIES);

        for (Relationship orderedCategory : orderedCategories) {
            Category category = CategoryNode.toCategory(orderedCategory.getEndNode());
            buyer.getOrderedCategories().add(category);
        }

        return buyer;

    }
}
