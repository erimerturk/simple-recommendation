package com.erim.domain.dto;

import com.erim.domain.Order;
import com.erim.domain.RelationshipTypes;
import com.erim.util.Neo4jHelper;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import java.util.HashSet;
import java.util.Set;

public class OrderNode extends BaseNode {

    public static final Label ORDER = DynamicLabel.label("Order");
    public static final String ORDER_ID = "orderId";

    private Long id;

    private String orderId;

    private Set<Node> orderItems = new HashSet<Node>();

    public Long getId() {
        return id;
    }

    @Override
    public Label getLabel() {
        return ORDER;
    }

    @Override
    public Node copyTo(Node node) {

        node.setProperty(ORDER_ID, orderId);

        for(Node endNode : orderItems){
            createOrderItemRelationship(node, endNode);
        }

        return node;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Set<Node> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(Set<Node> orderItems) {
        this.orderItems = orderItems;
    }

    public static  String getOrderId(Node order) {
        return (String)order.getProperty(ORDER_ID);
    }

    public static boolean hasOrderItemRel(Node start, Node end) {

        Iterable<Relationship> relationships = start.getRelationships(RelationshipTypes.ORDER_ITEM);

        for(Relationship relationship : relationships){
            if(relationship.getEndNode().equals(end)){
                return true;
            }
        }

        return false;
    }

    public static void createOrderItemRelationship(Node startNode, Node endNode) {
        Neo4jHelper.createRelationship(startNode, endNode, RelationshipTypes.ORDER_ITEM);
    }

    public static Order toOrder(Node node) {
        Order order = new Order();
        order.setId(node.getId());
        order.setOrderId((String) node.getProperty(ORDER_ID));
        return order;
    }
}
