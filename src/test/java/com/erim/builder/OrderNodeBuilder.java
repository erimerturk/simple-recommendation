package com.erim.builder;

import com.erim.domain.dto.OrderNode;
import org.apache.commons.lang.math.RandomUtils;
import org.neo4j.graphdb.Node;

import java.util.HashSet;
import java.util.Set;

public class OrderNodeBuilder extends BaseBuilder<OrderNode, OrderNodeBuilder> {

    private String orderId = String.valueOf(RandomUtils.nextLong());
    private Set<Node> orderItems = new HashSet<Node>();

    @Override
    protected OrderNode doBuild() {

        OrderNode orderNode = new OrderNode();
        orderNode.setOrderId(orderId);
        orderNode.setOrderItems(orderItems);
        return orderNode;
    }

    public OrderNodeBuilder orderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public OrderNodeBuilder child(Node child) {
        orderItems.add(child);
        return this;
    }

}
