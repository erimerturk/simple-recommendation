package com.erim.builder;


import com.erim.domain.dto.BaseNode;
import com.erim.service.NodeSaver;
import org.neo4j.graphdb.Node;

public abstract class BaseBuilder<T extends BaseNode, B extends BaseBuilder<T, B>> {

    private Long id;
    private boolean deleted;

    @SuppressWarnings("unchecked")
    public B id(Long id) {
        this.id = id;
        return (B) this;
    }

    @SuppressWarnings("unchecked")
    public B deleted(boolean deleted) {
        this.deleted = deleted;
        return (B) this;
    }

    
	@SuppressWarnings("unchecked")

    public Long getId() {
        return id;
    }
    
    @SuppressWarnings("unchecked")
    public Node persist(NodeSaver service) {
        T toPersist = build();
        return service.save(toPersist);
    }


    public T build() {
        T baseEntity = doBuild();
        baseEntity.setId(id);
        baseEntity.setDeleted(deleted);
        return baseEntity;
    }

    protected abstract T doBuild();

}
