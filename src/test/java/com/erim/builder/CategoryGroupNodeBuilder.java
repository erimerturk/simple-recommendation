package com.erim.builder;

import com.erim.domain.dto.CategoryGroupNode;
import com.erim.domain.dto.CategoryNode;
import org.apache.commons.lang.math.RandomUtils;

import java.util.HashSet;
import java.util.Set;

public class CategoryGroupNodeBuilder extends BaseBuilder<CategoryGroupNode, CategoryGroupNodeBuilder> {

    private String categoryId = String.valueOf(RandomUtils.nextLong());
    private Set<CategoryNode> childs = new HashSet<CategoryNode>();

    @Override
    protected CategoryGroupNode doBuild() {

        CategoryGroupNode node = new CategoryGroupNode();
        node.setCategoryGroupId(categoryId);
        node.setChildCategories(childs);
        return node;
    }

    public CategoryGroupNodeBuilder categoryGroupId(String categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public CategoryGroupNodeBuilder child(CategoryNode child) {
        childs.add(child);
        return this;
    }

}
