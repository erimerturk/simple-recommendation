package com.erim.builder;


import com.erim.domain.dto.BuyerNode;
import org.apache.commons.lang.math.RandomUtils;
import org.neo4j.graphdb.Node;

import java.util.HashSet;
import java.util.Set;

public class BuyerNodeBuilder extends BaseBuilder<BuyerNode, BuyerNodeBuilder> {

    private String buyerId = String.valueOf(RandomUtils.nextLong());
    private Set<Node> orders = new HashSet<Node>();
    private Set<Node> orderedCategories = new HashSet<Node>();
    private Set<Node> recommendedSkus = new HashSet<Node>();
    private Node genderNode;

    @Override
    protected BuyerNode doBuild() {

        BuyerNode buyerNode = new BuyerNode();
        buyerNode.setBuyerId(this.buyerId);
        buyerNode.setOrders(orders);
        buyerNode.setOrderedCategories(orderedCategories);
        buyerNode.setRecommendedSkus(recommendedSkus);
        buyerNode.setGenderNode(genderNode);
        return buyerNode;
    }

    public BuyerNodeBuilder buyerId(String buyerId) {
        this.buyerId = buyerId;
        return this;
    }

    public BuyerNodeBuilder orders(Node order) {
        this.orders.add(order);
        return this;
    }

    public BuyerNodeBuilder orderedCategories(Node categoryNode) {
        this.orderedCategories.add(categoryNode);
        return this;
    }

    public BuyerNodeBuilder recommendedSku(Node categoryNode) {
        this.recommendedSkus.add(categoryNode);
        return this;
    }

    public BuyerNodeBuilder gender(Node genderNode) {
        this.genderNode = genderNode;
        return this;
    }
}
