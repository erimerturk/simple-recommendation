package com.erim.builder;

import com.erim.domain.dto.CategoryNode;
import org.apache.commons.lang.math.RandomUtils;
import org.neo4j.graphdb.Node;

import java.util.HashSet;
import java.util.Set;

public class CategoryNodeBuilder extends BaseBuilder<CategoryNode, CategoryNodeBuilder> {

    private String categoryId = String.valueOf(RandomUtils.nextLong());
    private Set<Node> childs = new HashSet<Node>();

    @Override
    protected CategoryNode doBuild() {

        CategoryNode categoryNode = new CategoryNode();
        categoryNode.setCategoryId(categoryId);
        categoryNode.setChildCategories(childs);
        return categoryNode;
    }

    public CategoryNodeBuilder categoryId(String categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public CategoryNodeBuilder child(Node child) {
        childs.add(child);
        return this;
    }

}
