package com.erim.builder;

import com.erim.domain.dto.ProductNode;
import org.apache.commons.lang.math.RandomUtils;
import org.neo4j.graphdb.Node;

public class ProductNodeBuilder extends BaseBuilder<ProductNode, ProductNodeBuilder> {

    private String productId = String.valueOf(RandomUtils.nextLong());
    private Node categoryNode;

    @Override
    protected ProductNode doBuild() {

        ProductNode productNode = new ProductNode();
        productNode.setSkuId(this.productId);
        productNode.setCategory(categoryNode);
        return productNode;
    }

    public ProductNodeBuilder skuId(String skuId) {
        this.productId = skuId;
        return this;
    }

    public ProductNodeBuilder category(Node categoryNode) {
        this.categoryNode = categoryNode;
        return this;
    }
}
