package com.erim.service;

import com.erim.BaseIntegration;
import com.erim.builder.BuyerNodeBuilder;
import com.erim.builder.CategoryNodeBuilder;
import com.erim.builder.ProductNodeBuilder;
import com.erim.domain.RelationshipTypes;
import com.erim.domain.dto.BuyerNode;
import com.erim.util.Neo4jHelper;
import org.junit.Test;
import org.neo4j.graphdb.Node;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class BuyerServiceTest extends BaseIntegration {

  @Autowired
  private BuyerService buyerService;

    @Test
    public void shouldSaveWithBuyerId() {


        String buyerId = "123asd";
        Node buyerNode = new BuyerNodeBuilder().buyerId(buyerId).persist(nodeSaver);

        Node buyerNodeFromNeo = buyerService.findBy(buyerId);

        assertThat((String)buyerNode.getProperty(BuyerNode.BUYER_ID), equalTo((String)buyerNodeFromNeo.getProperty(BuyerNode.BUYER_ID)));
        assertThat((String)buyerNode.getProperty(BuyerNode.BUYER_ID), equalTo(buyerId));

    }

    @Test
    public void shouldSaveWithOrderedCategories() {

        Node category1 = new CategoryNodeBuilder().categoryId("123xxx").persist(nodeSaver);
        Node category2 = new CategoryNodeBuilder().categoryId("123yyy").persist(nodeSaver);

        String buyerId = "123asd";
        Node buyerNode = new BuyerNodeBuilder().orderedCategories(category1).orderedCategories(category2).buyerId(buyerId).persist(nodeSaver);

        Node buyerNodeFromNeo = buyerService.findBy(buyerId);

        assertThat((String)buyerNode.getProperty(BuyerNode.BUYER_ID), equalTo((String)buyerNodeFromNeo.getProperty(BuyerNode.BUYER_ID)));
        assertThat((String)buyerNode.getProperty(BuyerNode.BUYER_ID), equalTo(buyerId));

        assertTrue(BuyerNode.hasOrderedCategoryRel(buyerNodeFromNeo, category1));
        assertTrue(BuyerNode.hasOrderedCategoryRel(buyerNodeFromNeo, category2));

    }

    @Test
    public void shouldCreateRecommendedRelation() {

        Node category1 = new CategoryNodeBuilder().categoryId("123xxx").persist(nodeSaver);
        Node category2 = new CategoryNodeBuilder().categoryId("123yyy").persist(nodeSaver);

        Node sku1 = new ProductNodeBuilder().category(category1).persist(nodeSaver);
        Node sku2 = new ProductNodeBuilder().category(category2).persist(nodeSaver);

        String buyerId = "123asd";
        Node buyerNode = new BuyerNodeBuilder().recommendedSku(sku2).orderedCategories(category1).orderedCategories(category2).buyerId(buyerId).persist(nodeSaver);

        Node buyerNodeFromNeo = buyerService.findBy(buyerId);

        assertThat((String)buyerNode.getProperty(BuyerNode.BUYER_ID), equalTo((String)buyerNodeFromNeo.getProperty(BuyerNode.BUYER_ID)));
        assertThat((String)buyerNode.getProperty(BuyerNode.BUYER_ID), equalTo(buyerId));

        assertTrue(BuyerNode.hasRecommendedSkudRel(buyerNodeFromNeo, sku2));

    }

    @Test
    public void shouldCreateOrderedRelation() {

        Node category1 = new CategoryNodeBuilder().categoryId("123xxx").persist(nodeSaver);
        Node category2 = new CategoryNodeBuilder().categoryId("123yyy").persist(nodeSaver);

        Node sku1 = new ProductNodeBuilder().category(category1).persist(nodeSaver);
        Node sku2 = new ProductNodeBuilder().category(category2).persist(nodeSaver);

        String buyerId = "123asd";
        Node buyerNode = new BuyerNodeBuilder().orders(sku1).orderedCategories(category1).orderedCategories(category2).buyerId(buyerId).persist(nodeSaver);

        Node buyerNodeFromNeo = buyerService.findBy(buyerId);

        assertThat((String)buyerNode.getProperty(BuyerNode.BUYER_ID), equalTo((String)buyerNodeFromNeo.getProperty(BuyerNode.BUYER_ID)));
        assertThat((String)buyerNode.getProperty(BuyerNode.BUYER_ID), equalTo(buyerId));

        assertTrue(BuyerNode.hasOrderedProductRel(buyerNodeFromNeo, sku1));

    }

    @Test
    public void shouldCreateOrderedRelationWithCounter() {

        Node category1 = new CategoryNodeBuilder().categoryId("123xxx").persist(nodeSaver);
        Node category2 = new CategoryNodeBuilder().categoryId("123yyy").persist(nodeSaver);

        Node sku1 = new ProductNodeBuilder().category(category1).persist(nodeSaver);
        Node sku2 = new ProductNodeBuilder().category(category2).persist(nodeSaver);

        String buyerId = "123asd";
        Node buyerNode = new BuyerNodeBuilder().orders(sku1).orders(sku2).orderedCategories(category1).orderedCategories(category2).buyerId(buyerId).persist(nodeSaver);

        Node buyerNodeFromNeo = buyerService.findBy(buyerId);

        assertThat((String)buyerNode.getProperty(BuyerNode.BUYER_ID), equalTo((String)buyerNodeFromNeo.getProperty(BuyerNode.BUYER_ID)));
        assertThat((String)buyerNode.getProperty(BuyerNode.BUYER_ID), equalTo(buyerId));

        assertTrue(BuyerNode.hasOrderedProductRel(buyerNodeFromNeo, sku1));
        assertThat(Neo4jHelper.getRelationshipCounter(buyerNodeFromNeo, sku1, RelationshipTypes.ORDERED_PRODUCT), equalTo(1));

    }


}
