package com.erim.service;

import com.erim.BaseIntegration;
import com.erim.builder.OrderNodeBuilder;
import com.erim.builder.ProductNodeBuilder;
import com.erim.domain.dto.OrderNode;
import org.junit.Test;
import org.neo4j.graphdb.Node;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class OrderServiceTest extends BaseIntegration {

    @Autowired
    private OrderService orderService;

    @Test
    public void shouldCreateOrderItemRelation() {

        Node order = new OrderNodeBuilder().persist(getNeo4j());

        Node product = new ProductNodeBuilder().persist(getNeo4j());

        orderService.createOrderItemRelation(order, product);

        Node fromNEO = orderService.findOrderBy(OrderNode.getOrderId(order));
        
        assertThat(order, equalTo(fromNEO));
        assertThat(OrderNode.hasOrderItemRel(fromNEO, product), equalTo(true));


    }
}
