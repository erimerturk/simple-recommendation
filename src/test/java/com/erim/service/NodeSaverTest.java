package com.erim.service;

import com.erim.BaseIntegration;
import com.erim.builder.BuyerNodeBuilder;
import com.erim.builder.CategoryNodeBuilder;
import com.erim.domain.dto.BuyerNode;
import com.erim.domain.dto.CategoryNode;
import org.junit.Test;
import org.neo4j.graphdb.Node;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class NodeSaverTest extends BaseIntegration {

    @Autowired
    private BuyerService buyerService;

    @Test
    public void shouldSaveWithProperty() {

        String buyerId = "123asd";
        Node buyerNode = new BuyerNodeBuilder().buyerId(buyerId).persist(nodeSaver);

        Node buyerNodeFromNeo = buyerService.findBy(buyerId);

        assertThat((String)buyerNode.getProperty(BuyerNode.BUYER_ID), equalTo((String)buyerNodeFromNeo.getProperty(BuyerNode.BUYER_ID)));
        assertThat((String)buyerNode.getProperty(BuyerNode.BUYER_ID), equalTo(buyerId));

    }

    @Test
    public void shouldSaveWithRelationship() {

        Node category1 = new CategoryNodeBuilder().categoryId("123xxx").persist(nodeSaver);
        Node category2 = new CategoryNodeBuilder().categoryId("123yyy").persist(nodeSaver);

        Node parent = new CategoryNodeBuilder().child(category1).child(category2).categoryId("parent").persist(nodeSaver);

        String buyerId = "123asd";
        Node buyerNode = new BuyerNodeBuilder().orderedCategories(category1).orderedCategories(category2).buyerId(buyerId).persist(nodeSaver);

        Node buyerNodeFromNeo = buyerService.findBy(buyerId);

        assertThat((String)buyerNode.getProperty(BuyerNode.BUYER_ID), equalTo((String)buyerNodeFromNeo.getProperty(BuyerNode.BUYER_ID)));
        assertThat((String)buyerNode.getProperty(BuyerNode.BUYER_ID), equalTo(buyerId));

        assertTrue(BuyerNode.hasOrderedCategoryRel(buyerNodeFromNeo, category1));
        assertTrue(BuyerNode.hasOrderedCategoryRel(buyerNodeFromNeo, category2));

        assertTrue(CategoryNode.hasChildRel(parent, category1));
        assertTrue(CategoryNode.hasChildRel(parent, category2));

    }

}
