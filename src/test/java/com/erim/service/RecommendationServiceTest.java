package com.erim.service;

import com.erim.BaseIntegration;
import com.erim.builder.BuyerNodeBuilder;
import com.erim.builder.GenderNodeBuilder;
import com.erim.builder.ProductNodeBuilder;
import com.erim.domain.dto.BaseNode;
import com.erim.domain.dto.BuyerNode;
import com.erim.domain.dto.GenderNode;
import com.erim.domain.dto.GenderType;
import com.erim.domain.dto.ProductNode;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.neo4j.graphdb.Node;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class RecommendationServiceTest extends BaseIntegration {

    @Autowired
    private RecommendationService recommendationService;

    @Test
    public void shouldRecommendByProductId() {

        Node sku1 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku2 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku3 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku4 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku5 = new ProductNodeBuilder().persist(getNeo4j());

        Node b1 = new BuyerNodeBuilder().orders(sku1).orders(sku2).orders(sku3).persist(getNeo4j());
        Node b2 = new BuyerNodeBuilder().orders(sku2).orders(sku4).persist(getNeo4j());
        Node b3 = new BuyerNodeBuilder().orders(sku1).orders(sku2).orders(sku5).persist(getNeo4j());

        List<String> fromNEO = recommendationService.recommendByProductId(ProductNode.getProductId(sku2));

        assertThat(fromNEO, hasItem(ProductNode.getProductId(sku1)));

    }

    @Test
    public void shouldRecommendByBuyerTransactionHistory() {

        Node sku1 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku2 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku3 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku4 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku5 = new ProductNodeBuilder().persist(getNeo4j());

        Node b1 = new BuyerNodeBuilder().orders(sku1).orders(sku2).orders(sku3).persist(getNeo4j());
        Node b2 = new BuyerNodeBuilder().orders(sku2).orders(sku4).persist(getNeo4j());
        Node b3 = new BuyerNodeBuilder().orders(sku1).orders(sku2).orders(sku5).persist(getNeo4j());

       boolean deleted = (Boolean)sku1.getProperty(BaseNode.DELETED);

        List<String> fromNEO = recommendationService.recommendByBuyerTransactionHistory(BuyerNode.getBuyerId(b1));

        assertThat(fromNEO, hasItem(ProductNode.getProductId(sku4)));
        assertThat(fromNEO, hasItem(ProductNode.getProductId(sku5)));

    }

    @Test
    public void shouldRecommendByBuyerWatchList() {

        Node sku1 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku2 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku3 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku4 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku5 = new ProductNodeBuilder().persist(getNeo4j());

        Node b1 = new BuyerNodeBuilder().orders(sku1).orders(sku2).orders(sku3).persist(getNeo4j());
        Node b2 = new BuyerNodeBuilder().orders(sku2).orders(sku4).persist(getNeo4j());
        Node b3 = new BuyerNodeBuilder().orders(sku1).orders(sku2).orders(sku5).persist(getNeo4j());

        List<String> fromNEO = recommendationService.recommendByBuyerWatchList(BuyerNode.getBuyerId(b1),
                Lists.newArrayList(ProductNode.getProductId(sku2), ProductNode.getProductId(sku4)));

        assertThat(fromNEO, hasItem(ProductNode.getProductId(sku5)));
        assertThat(fromNEO.size(), equalTo(1));

    }

    @Test
    public void shouldRecommendBasedOnGender() {

        Node sku1 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku2 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku3 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku4 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku5 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku6 = new ProductNodeBuilder().persist(getNeo4j());

        Node female = new GenderNodeBuilder().gender(GenderType.FEMALE).persist(getNeo4j());

        Node b1 = new BuyerNodeBuilder().gender(female).orders(sku1).orders(sku2).orders(sku3).persist(getNeo4j());
        Node b2 = new BuyerNodeBuilder().gender(female).orders(sku2).orders(sku4).persist(getNeo4j());
        Node b3 = new BuyerNodeBuilder().gender(female).orders(sku2).orders(sku4).orders(sku5).persist(getNeo4j());
        Node b4 = new BuyerNodeBuilder().orders(sku1).orders(sku2).orders(sku5).orders(sku6).persist(getNeo4j());

        List<String> fromNEO = recommendationService.recommendByBuyerTransactionHistoryBasedOnGender(BuyerNode.getBuyerId(b1), GenderNode.getGenderType(female));

        assertThat(fromNEO.indexOf(ProductNode.getProductId(sku4)) < fromNEO.indexOf(ProductNode.getProductId(sku5)), equalTo(true));
        assertThat(fromNEO, hasItem(ProductNode.getProductId(sku4)));
        assertThat(fromNEO, not(hasItem(ProductNode.getProductId(sku6))));

    }

}
