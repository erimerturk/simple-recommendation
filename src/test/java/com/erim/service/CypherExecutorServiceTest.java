package com.erim.service;

import com.erim.BaseIntegration;
import com.erim.builder.BuyerNodeBuilder;
import com.erim.builder.ProductNodeBuilder;
import com.erim.domain.dto.ProductNode;
import com.google.common.collect.Maps;
import org.junit.Test;
import org.neo4j.graphdb.Node;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;

import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;

public class CypherExecutorServiceTest extends BaseIntegration {

    @Autowired
    private CypherExecutorService executorService;

    @Test
    public void shouldExecuteQueryByParams() {


        Node sku1 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku2 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku3 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku4 = new ProductNodeBuilder().persist(getNeo4j());
        Node sku5 = new ProductNodeBuilder().persist(getNeo4j());

        Node b1 = new BuyerNodeBuilder().orders(sku1).orders(sku2).orders(sku3).persist(getNeo4j());
        Node b2 = new BuyerNodeBuilder().orders(sku2).orders(sku4).persist(getNeo4j());
        Node b3 = new BuyerNodeBuilder().orders(sku1).orders(sku2).orders(sku5).persist(getNeo4j());

        HashMap<String,Object> params = Maps.newHashMap();
        params.put("productId", ProductNode.getProductId(sku2));

        List<String> fromNEO = executorService.executeWithParams("recommendByProductId", params, "skuId");
        assertThat(fromNEO, hasItem(ProductNode.getProductId(sku1)));

    }
}
