package com.erim;

import com.erim.service.NodeSaver;
import com.erim.util.Neo4jHelper;
import org.junit.After;
import org.junit.runner.RunWith;
import org.neo4j.graphdb.GraphDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration(locations = "classpath:/app-context-test.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class BaseIntegration {

    @Autowired
    protected GraphDatabaseService graphDatabaseService;

    @Autowired
    protected NodeSaver nodeSaver;

    public NodeSaver getNeo4j() {
        return nodeSaver;
    }

//    @Before
//    public void prepareTestDatabase()
//    {
//        graphDatabaseService = new TestGraphDatabaseFactory().newImpermanentDatabase();
//    }
//
//    @After
//    public void destroyTestDatabase()
//    {
//        if (neo4j != null) {
//            neo4j.shutdown();
//        }
//
//    }


//    @Rollback(false)
    @BeforeTransaction
    @After
    public void cleanUpGraph() {
        Neo4jHelper.cleanDb(graphDatabaseService);
    }
}
